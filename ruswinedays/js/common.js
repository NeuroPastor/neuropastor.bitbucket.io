$(function() {

	var adult = $.cookie('adult');
	//console.log(adult);
	if(adult == undefined){
		$(".forAdults").fadeIn();
		$("body").addClass("overflow");
	}
	$(".forAdults .agree").click(function(e){
		e.preventDefault();
		$(".forAdults").fadeOut();
		$("body").removeClass("overflow");	
		$.cookie("adult", true, { expires: 7 });
	})


	var owl = $('#shops .items');
	owl.owlCarousel({
	    loop:true,
	    margin:10,
	    dots:true,
	    nav:false,
	    //navElement:
	    dotsContainer:'.pagi',
	    dotsEach:true,
	    items:1,
	    autoplay:true,
	    autoplayTimeout:2000,
	    //fluidSpeed:2,
	    smartSpeed:2700,
	    //autoplaySpeed: 250,
	    responsive:{
	        0:{
	            items:2
	        },

	       	500:{
	            items:2
	        }
	    }
	})
	$(".nav-wrapper .next").click(function(){
		 owl.trigger('next.owl.carousel');
	});
	$(".nav-wrapper .prev").click(function(){
		 owl.trigger('prev.owl.carousel');
	})

	var offsetTop = {};
	
	var headerHeight = $(".header").outerHeight();
	var navbarHeight = $(".navbar").outerHeight();
	if($(".navbar").offset() !== undefined){
		offsetTop.navbarPos = $(".navbar").offset().top;
	}
	if($(".ruswinedays__block--fixed img").offset() !== undefined){
		offsetTop.graySlogan = $(".ruswinedays__block--fixed img").offset().top;
	}
	//ruswinedays__block--fixed
	offsetTop.footer = $(".footer").offset().top;
	offsetTop.social = $(".social").offset().top;
	$(window).on("scroll", function(){
		var scrolledTop = $(window).scrollTop();	
		var marginTop = 0;
		if($(window).width() > 991){
			marginTop = 30;
		}
		if($(".ruswinedays__block--fixed img").offset() !== undefined){
			if(scrolledTop >= offsetTop.graySlogan && (scrolledTop + 220 + marginTop) < offsetTop.footer){
				$(".ruswinedays__block--fixed img").offset({top:scrolledTop});
			} else if(scrolledTop <= offsetTop.graySlogan){
				$(".ruswinedays__block--fixed img").css({"z-index":1}).offset({top:offsetTop.graySlogan});
			}
			if($(".ruswinedays__block--fixed img").offset().top >= headerHeight){
				$(".ruswinedays__block--fixed img").css({"z-index":-1});
			} else {
				$(".ruswinedays__block--fixed img").css({"z-index":1});
			}

		}
		if(scrolledTop >=offsetTop.social && (scrolledTop + 220 + marginTop) < offsetTop.footer){
			$(".social").offset({top:scrolledTop+60});
		} else if(scrolledTop <= offsetTop.social){
			$(".social").offset({top:offsetTop.social});
		}

		if(scrolledTop >= offsetTop.navbarPos && ( (scrolledTop + navbarHeight +marginTop + 90) < offsetTop.footer) && scrolledTop >= headerHeight+60) {
			$(".navbar").offset({top:scrolledTop+marginTop});
		} else if(scrolledTop <= offsetTop.navbarPos){
			$(".navbar").offset({top:offsetTop.navbarPos});
		}

		if($(".navbar").offset() !== undefined && !$(".navbar").hasClass("spy-false")){
			try {
				$(".navbar a").each(function(imdx,el){
						var anchor1 = $(this).attr("href");
						//if($(anchor).offset().top - ($(window).outerHeight()/.9) + $(anchor).outerHeight() < scrolledTop){
						//if(scrolledTop + $(".footer").outerHeight()/10 >= $("html").outerHeight() -  $(window).outerHeight() ) {
						
					    if($("html, body").outerHeight() <= scrolledTop + $(window).outerHeight() + $(".footer").outerHeight()/4) {
							$(".navbar a.showThis").removeClass("showThis");
							$(".navbar li:last-child a").addClass('showThis');
						} 
						if(scrolledTop >= $(anchor1).offset().top - $(window).outerHeight()/2 + $(anchor1).outerHeight()/2){
							$(".navbar a").removeClass("showThis");
							$(this).addClass('showThis');
						} 
						console.log($("html, body").outerHeight() + " " + scrolledTop + " " + $(window).outerHeight());
				
				})
			} catch(e){
				//console.log(e);
			}
		} else {
			return false;
		}
		// console.log("scroll"+scrollTop);
		// console.log("html"+$("html").outerHeight() );
		// console.log("window" + $(window).outerHeight());
		//var h = $("html").outerHeight() -  $(window).outerHeight();
		//console.log("math: "+ h);


		
	});

	$("a[href^='#']").click(function(){
		var anchor = $(this).attr("href");
		$("html").animate({scrollTop: $(anchor).offset().top - $(window).outerHeight()/2 +  $(anchor1).outerHeight()/2}, 300, "swing");
		// $(".navbar a").removeClass("showThis");
		// $(this).addClass("showThis");
	});



	$("#feedback form").submit(function(form){
		form.preventDefault();
		var err = 0;

		$("#feedback form input").each(function(){
            if($(this).val()=='' && $(this).attr("type") !== 'hidden'){
                err++;
                $(this).after('<span class="error">Все поля обязательны для заполнения!</span>')
            }
        });
        if(err == 0){
			var name = $("#feedback form input[name=name]").val();
			var email = $("#feedback form input[name=email]").val();
			var phone = $("#feedback form input[name=phone]").val();
			var question = $("#feedback form textarea[name=question]").val();
			$("form button[type=submit]").remove();
			
            $("#feedback form .success").fadeIn().text("Идет отправка..."); 
            console.log("name="+name+"&email="+email+"&phone="+phone+"&question="+question);
            $.ajax({
                url: "./sendmail.php",
                type: "POST",
                data:"name="+name+"&email="+email+"&phone="+phone+"&question="+question,
                success:function(data){
                   	$("#feedback form .success").text(data); 
                	
                }

            })
        }
		
	})


});
